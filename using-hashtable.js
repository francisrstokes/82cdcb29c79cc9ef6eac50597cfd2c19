const HashTable = require('./HashTable.js');

// Create a hashtable with 2048 buckets (unique slots)
const ht = new HashTable(2048);

// Keys can most javascript primitives
ht.set({ x: 210 * 2}, 1);
ht.set([1,2,3], 2);
ht.set('cba', 3);

// Retrieving doesn't require references - deeply equivilent structures will hash to the same value
const values = [
  ht.get({x: 420}),                 // 1
  ht.get([2,4,6].map(x => x / 2)),  // 2
  ht.get('cba'),                    // 3
];