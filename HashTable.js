class HashTable {
  constructor(bucketSize = 1024) {
    this._bucketSize = bucketSize;
    this._data = new Array(bucketSize);
  }

  hashKey(key) {
    const h = JSON.stringify(key, Object.keys(key).sort())
      .split('')
      .reduce((acc, cur, i) => acc + cur.charCodeAt(0) * (i+1), 0);
    return h % this._bucketSize;
  }
  set(key, value) {
    return this._data[this.hashKey(key)] = value;
  }
  get(key) {
    return this._data[this.hashKey(key)];
  }
}

module.exports = HashTable;