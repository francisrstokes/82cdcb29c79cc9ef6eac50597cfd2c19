class ConflictResolvingHashTable {
  constructor(bucketSize = 1024) {
    this._bucketSize = bucketSize;
    this._data = new Array(bucketSize);
  }

  hashKey(key) {
    const h = JSON.stringify(key, Object.keys(key).sort())
      .split('')
      .reduce((acc, cur, i) => acc + cur.charCodeAt(0) * (i+1), 0);
    return h % this._bucketSize;
  }

  set(key, value) {
    const hk = this.hashKey(key);
    const bucket = this._data[hk];
    
    if (Array.isArray(bucket)) {
      const idx = bucket.findIndex(([storedKey]) => key === storedKey);
      if (idx === -1) {
        bucket.push([key, value]);
      } else {
        bucket[idx][1] = value;
      }
    } else {
      this._data[hk] = [[key, value]];
    }
  }

  get(key) {
    const bucket = this._data[this.hashKey(key)];
    if (Array.isArray(bucket)) {
      const keyValue = bucket.find(([storedKey]) => key === storedKey);
      return keyValue ? keyValue[1] : undefined;
    }
  }
}